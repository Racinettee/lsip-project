(in-package #:org.trialtest)

(defvar *black* (gamekit:vec4 0 0 0 1))
(defvar *origin* (gamekit:vec2 0 0))

(gamekit:defgame example () ())

(defmethod gamekit:draw ((this example))
  (gamekit:draw-rect *origin* 100 100 :fill-paint *black*)
  (gamekit:draw-text "Hello, gamedev!" (gamekit:vec2 240.0 240.0)))

(defun launch () (gamekit:start 'example))
