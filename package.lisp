(defpackage #:org.trialtest
  (:use :cl :trivial-gamekit)
  (:shadow #:main #:launch)
  (:export #:main #:launch))
